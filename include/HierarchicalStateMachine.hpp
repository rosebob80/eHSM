/*******************************************************************************
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Ernesto Cruz Olivera (ecruzolivera@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 *all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

/**
 ******************************************************************************
 * @file    HierarchicalStateMachine.hpp
 * @author  Ing. Manuel Alejandro Linares Paez
 * @version 1.0
 * @date    Apr 14, 2016
 * @brief   Class for the state machine
 * @bug     No known bugs.
 ******************************************************************************
 */

#ifndef HIERARCHICALSTATEMACHINE_HPP
#define HIERARCHICALSTATEMACHINE_HPP

#include "State.hpp"
#include "Utils.hpp"

#ifndef HSM_MAX_NEST_STATES
#  define HSM_MAX_NEST_STATES 16
#endif

namespace eHSM
{
/**
 * @brief The HierarchicalStateMachine class represents a hierarchycal state
 * machine
 */
class HierarchicalStateMachine
{
 public:
  /**
   * @brief Public constructor
   */
  HierarchicalStateMachine() : isRunning_(false), currentState_(E_NULLPTR), initialState_(E_NULLPTR) {}

  /**
   * @brief Public constructor
   * @param initial Initial state
   */
  explicit HierarchicalStateMachine(eHSM::State &initialState)
      : isRunning_(false), currentState_(E_NULLPTR), initialState_(&initialState)
  {
  }

  /**
   * @brief Function called to process the incoming events by the state machine
   * @param event Event
   */
  void dispatch(State::EventId_t eventId)
  {
    if(isRunning_)
    {
      State::EventHandlerType retValue;
      State *                 superstate;
      State *                 thisState;
      State *                 exitPath[HSM_MAX_NEST_STATES];
      std::uint32_t           exitIndex = 0;

      superstate = currentState_;
      // Pass event to state hierarchy
      do
      {
        assert(exitIndex < HSM_MAX_NEST_STATES);
        thisState           = superstate;
        retValue            = thisState->handleEvent(eventId);
        exitPath[exitIndex] = thisState;
        exitIndex++;
        superstate = thisState->superstate();
      } while((retValue == State::EventIgnored) && (superstate != E_NULLPTR));

      // In case the event was ignored call default events
      if(retValue == State::EventIgnored)
      {
        for(std::uint32_t i = 0; i < exitIndex; i++)
        {
          retValue = exitPath[i]->handleDefaultEvent(eventId);
          if(retValue == State::TriggerTransition || retValue == State::TriggerActionAndTransition)
          {
            exitIndex = i + 1;
            thisState = exitPath[i];
            break;
          }
        }
      }

      if(retValue == State::TriggerTransition || retValue == State::TriggerActionAndTransition)
      {
        // Execute exit actions of down states to the source of transition
        for(std::uint32_t i = 0; i < exitIndex - 1; i++)
        {
          exitPath[i]->signalExited.notify();
          exitPath[i]->onExit();
        }
        currentState_ = thisState;
        exitIndex     = 0;
        // Create the exit path from the source of transition to the top state
        do
        {
          assert(exitIndex < HSM_MAX_NEST_STATES);
          exitPath[exitIndex] = thisState;
          exitIndex++;
          thisState = thisState->superstate();
        } while(thisState != E_NULLPTR);

        State *       entryPath[HSM_MAX_NEST_STATES];
        std::uint32_t entryIndex = 0, endExitPath = exitIndex;
        thisState              = currentState_->nextStatePtr_;
        bool superStateFounded = false;
        // Create the entry path from the target of transition to a common
        // superstate with the exit path
        while(!superStateFounded && (thisState != E_NULLPTR))
        {
          entryPath[entryIndex] = thisState;
          entryIndex++;
          for(std::uint32_t i = 0; i < exitIndex; i++)
          {
            if(thisState == exitPath[i])
            {
              superStateFounded = true;
              endExitPath       = i;
              entryIndex--;
              break;
            }
          }
          thisState = thisState->superstate();
        }

        // Call exit and entry actions of both path
        for(std::uint32_t i = 0; i < endExitPath; i++)
        {
          exitPath[i]->signalExited.notify();
          exitPath[i]->onExit();
        }
        for(int i = entryIndex - 1; i >= 0; i--)
        {
          entryPath[i]->signalEntered.notify();
          entryPath[i]->onEntry();
        }

        // Set current state
        currentState_ = currentState_->nextStatePtr_;

        // Check for initial transitions
        while(currentState_->hasInitialSubState())
        {
          currentState_ = currentState_->initialState();
          currentState_->signalEntered.notify();
          currentState_->onEntry();
        }
      }
    }
  }

  /**
   * @brief Function to init,start and restart the state machine
   */
  bool start()
  {
    if(initialState_ == E_NULLPTR)
    {
      return false;
    }
    if(!isRunning_ && currentState_ == E_NULLPTR)
    {
      State *path[HSM_MAX_NEST_STATES];
      State *super;
      super               = initialState_;
      std::uint32_t index = 0;

      do
      {
        path[index] = super;
        index++;
        super = super->superstate();
      } while(super && (index < HSM_MAX_NEST_STATES));

      for(index--; index > 0; index--)
      {
        path[index]->signalEntered.notify();
        path[index]->onEntry();
      }
      path[0]->signalEntered.notify();
      path[0]->onEntry();

      currentState_ = initialState_;

      // Check for initial transitions
      while(currentState_->hasInitialSubState())
      {
        currentState_ = currentState_->initialState();
        currentState_->signalEntered.notify();
        currentState_->onEntry();
      }
    }
    return isRunning_ = true;
  }

  /**
   * @brief currentState
   * @return Pointer to the active state
   */
  State *currentState() const
  {
    return currentState_;
  }

  /**
   * @brief Stops the processing of events
   */
  void stop()
  {
    isRunning_ = false;
  }

  /**
   * @brief initialState
   * @return Pointer to the initial state of the state machine
   */
  State *initialState() const
  {
    return initialState_;
  }

  /**
   * @brief Sets the initial state of the state machine
   * @param initialState
   */
  void setInitialState(State &initialStateRef)
  {
    if(!isRunning_)
    {
      initialState_ = &initialStateRef;
    }
  }

 private:
  /**
   * @brief Determines if the state machine can process the evens
   */
  bool isRunning_;

  /**
   * @brief Pointer to the current active state
   */
  State *currentState_;

  /**
   * @brief  Pointer to the initial state
   */
  State *initialState_;

  E_DISABLE_COPY(HierarchicalStateMachine);

};  // HierarchicalStateMachine

}  // namespace eHSM

#endif  // HIERARCHICALSTATEMACHINE_HPP
