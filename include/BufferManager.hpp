/*******************************************************************************
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Ernesto Cruz Olivera (ecruzolivera@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 *all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#ifndef BUFFERMANAGER_HPP
#define BUFFERMANAGER_HPP

#include <cstdint>
#include <cstring>

#include "DmaBuffer.hpp"
#include "RingBuffer.hpp"

namespace eHSM
{
/**
 * @class BufferManager
 * Class that manage a set of Buffer objects to be used with a DMA
 * @tparam Type Template parameter tha specify the buffer type.
 * @tparam BUFFER_SIZE Template parameter tha specify the buffer lenght.
 * @tparam BUFFERS_NUMBER Template parameter tha specify the number of buffers.
 * @tparam BufferType Template parameter tha specify the buffer class, should be
 * one of the next list @link eContainer::Declare::LinearBuffer or
 * eContainer::Declare::RingBuffer.
 *
 * @note All memory in the object stack
 *
 * @code{.cpp}
 * const size_t BUFFER_SIZE = 256;
 * const size_t BUFFER_QTTY = 3;
 * BufferManager<uint16_t, BUFFER_SIZE, BUFFER_QTTY,
 * eContainer::Declare::LinearBuffer> buffer;
 * @endcode
 */
template <typename Type,
          std::size_t BUFFER_SIZE                           = 256,
          std::size_t BUFFERS_QUANTITY                      = 3,
          template <typename, std::size_t> class BufferType = ::eHSM::Declare::DmaBuffer>
class BufferManager
{
 public:
  /**
   * @brief BufferManager
   */
  BufferManager()
  {
    writeIndex_   = 0;
    readIndex_    = 0;
    writeCounter_ = 0;
    readCounter_  = 0;
  }

  /**
   * @brief write
   * write a single item to the end of the active buffer
   * @param itemRef Reference to the item
   * @retval 1 if success
   * @retval 0 if all buffer are full
   */
  std::size_t write(const Type &itemRef)
  {
    if(buffersArray_[writeIndex_].isFull())
    {
      return 0;  // if all buffers are full stop writting
    }
    buffersArray_[writeIndex_].write(itemRef);
    proccessActiveBuffer();  // if buffer full switch buffer
    return 1;
  }

  /**
   * @brief write
   * write a items array to the buffer colletion, if the array does fit in the
   * active buffer it is written in all the buffers
   * @param dataPtr
   * @param dataLen
   * @return the number of items that were written
   */
  std::size_t write(const Type *dataPtr, std::size_t dataLen)
  {
    if(buffersArray_[writeIndex_].isFull())
    {
      return 0;
    }
    std::size_t itemsWritten = 0;

    while(itemsWritten < dataLen)
    {
      // in each iteration write only the pending ones
      itemsWritten += buffersArray_[writeIndex_].write(dataPtr + itemsWritten, dataLen - itemsWritten);

      proccessActiveBuffer();  // if buffer full switch buffer
      if(buffersArray_[writeIndex_].isFull())
      {
        break;  // if next buffer is also full stop writting
      }
    }
    return itemsWritten;
  }

  /**
   * @brief nextBufferForWrite
   * Mark the active buffer as a ready to read, and swith to the next empty
   * buffer
   * @warning IMPROPER USE OF THIS METHOD COULD CAUSE A RACE CONDITION!!!!
   * @retval true if the next buffer is empty
   * @retval false if all buffers are full
   */
  bool nextBufferForWrite()
  {
    if(buffersEmptyAviable() == 0)
    {
      return false;
    }
    writeCounter_++;
    writeIndex_++;
    if(writeIndex_ == BUFFERS_QUANTITY)
    {
      writeIndex_ = 0;  // Comienza a escribir desde el principio.
    }
    return true;
  }

  /**
   * @brief nextBufferToBeRead
   * Makes the method buffer returns the next ready to be read buffer.
   * @retval true if there is a buffer ready for be read.
   * @retval false if all buffers are empty.
   * @warning This method mark the current ready to be readed buffer as readed!
   */
  bool nextBufferToBeRead()
  {
    bool retval = false;
    if(buffersFullAviable() == 0)
    {
      return false;
    }
    else if(buffersFullAviable() == 1)
    {
      retval = false;
    }
    else
    {
      retval = true;
    }
    readCounter_++;
    buffersArray_[readIndex_++].reset();
    if(readIndex_ == BUFFERS_QUANTITY)
    {
      readIndex_ = 0;
    }
    return retval;
  }

  /**
   * @brief buffersEmptyAviable
   * @return the number of buffers empty
   */
  std::size_t buffersEmptyAviable() const
  {
    return BUFFERS_QUANTITY - buffersFullAviable();
  }

  /**
   * @brief buffesReadyToBeRead
   * @return the number of buffers ready to be read
   */
  std::size_t buffersFullAviable() const
  {
    return writeCounter_ - readCounter_;
  }

  E_DECLARE_CONSTEXPR std::size_t buffersLenght() const
  {
    return BUFFER_SIZE;
  }
  /**
   * @brief resetAllBuffers
   * Reset the internal buffer system.
   */
  void reset()
  {
    for(unsigned index = 0; index < BUFFERS_QUANTITY; ++index)
    {
      buffersArray_[index].reset();
    }
    writeCounter_ = 0;
    readCounter_  = 0;
    writeIndex_   = 0;
    readIndex_    = 0;
  }

  /**
   * @brief buffer
   * @returns A pointer to the first Buffer that is ready to be readed,
   * NULL if all buffers are empty
   */
  BufferType<Type, BUFFER_SIZE> *buffer()
  {
    //        if (buffersFullAviable() == 0) {
    //            return NULL;
    //        }
    return &buffersArray_[readIndex_];
  }

 private:
  /**
   * @brief proccessActiveBuffer
   * Check if the active buffer is full, and if it's switch to next buffer
   * @retval true if the active buffer is full and was switched for the next
   * buffer.
   * @retval false if the active buffer wasn't full
   */
  bool proccessActiveBuffer()
  {
    bool retval = false;
    if(buffersArray_[writeIndex_].isFull())
    {
      writeIndex_++;
      if(writeIndex_ == BUFFERS_QUANTITY)
      {
        writeIndex_ = 0;  // Comienza a escribir desde el principio.
      }
      writeCounter_++;  // Incrementa el contador de buffers.
      retval = true;
    }
    return retval;
  }

  BufferType<Type, BUFFER_SIZE> buffersArray_[BUFFERS_QUANTITY];
  std::size_t                   readIndex_;   //!< always point to the first fill buffer.
  std::size_t                   writeIndex_;  //!< always point to the active buffer.
  volatile std::size_t          writeCounter_;
  volatile std::size_t          readCounter_;

  E_DISABLE_COPY(BufferManager);
};
}  // namespace eHSM
#endif  // BUFFERMANAGER_HPP
