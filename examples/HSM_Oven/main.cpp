#include <stdio.h>

#include "HierarchicalStateMachine.hpp"

using namespace eHSM;

void InternalLampOn()
{
  printf("InternalLampOn\r\n");
}

void InternalLampOff()
{
  printf("InternalLampOff\r\n");
}

void HeaterOn()
{
  printf("HeaterOn\r\n");
}

void HeaterOff()
{
  printf("HeaterOff\r\n");
}

void ArmAlarm()
{
  printf("Arm toasting alarm\r\n");
}

void DisarmAlarm()
{
  printf("Disarm toasting alarm\r\n");
}

void SetBakingTemperature()
{
  printf("Baking\r\n");
}

void TurnOffBaking()
{
  printf("Not Baking\r\n");
}

int main()
{
  enum Events
  {
    DoorOpen,
    DoorClose,
    DoToasting,
    DoBaking
  };
  const int DOOROPEN_STATE_EVENTS = 1;
  const int HEATING_STATE_EVENTS  = 3;
  const int TOASTING_STATE_EVENTS = 1;
  const int BAKING_STATE_EVENTS   = 1;

  Declare::State<DOOROPEN_STATE_EVENTS> doorOpenState;
  Declare::State<HEATING_STATE_EVENTS>  heatingState;
  Declare::State<TOASTING_STATE_EVENTS> toastingState;
  Declare::State<BAKING_STATE_EVENTS>   bakingState;
  HierarchicalStateMachine              hsm;
  // Connecting Entering and Exiting Signals
  doorOpenState.signalEntered.connect<InternalLampOn>();
  doorOpenState.signalExited.connect<InternalLampOff>();
  doorOpenState.addEvent(DoorClose, heatingState);

  heatingState.signalEntered.connect<HeaterOn>();
  heatingState.signalExited.connect<HeaterOff>();
  heatingState.addEvent(DoorOpen, doorOpenState);
  heatingState.addEvent(DoToasting, toastingState);
  heatingState.addEvent(DoBaking, bakingState);

  toastingState.signalEntered.connect<ArmAlarm>();
  toastingState.signalExited.connect<DisarmAlarm>();
  toastingState.setSuperstate(heatingState);

  bakingState.signalEntered.connect<SetBakingTemperature>();
  bakingState.signalExited.connect<TurnOffBaking>();
  bakingState.setSuperstate(heatingState);

  hsm.setInitialState(heatingState);
  hsm.start();
  hsm.dispatch(DoorOpen);
  hsm.dispatch(DoorClose);
  hsm.dispatch(DoToasting);
  hsm.dispatch(DoorOpen);
  hsm.dispatch(DoorClose);
  hsm.dispatch(DoBaking);
  hsm.dispatch(DoorOpen);
}
