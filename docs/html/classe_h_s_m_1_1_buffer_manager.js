var classe_h_s_m_1_1_buffer_manager =
[
    [ "BufferManager", "classe_h_s_m_1_1_buffer_manager.html#a133fd51fb56d9351fc26354c89deb448", null ],
    [ "buffer", "classe_h_s_m_1_1_buffer_manager.html#a309ae63fbf804875b243a1de941fc39e", null ],
    [ "buffersEmptyAviable", "classe_h_s_m_1_1_buffer_manager.html#a0926b49b5524f4baefd489e1810fb668", null ],
    [ "buffersFullAviable", "classe_h_s_m_1_1_buffer_manager.html#ad2f291dcf548a4680045488902fa9497", null ],
    [ "buffersLenght", "classe_h_s_m_1_1_buffer_manager.html#a8e1acfb4dc4f516054a9a093ae3296cf", null ],
    [ "nextBufferForWrite", "classe_h_s_m_1_1_buffer_manager.html#ae9670d2b2c42a43513ff78253b98272f", null ],
    [ "nextBufferToBeRead", "classe_h_s_m_1_1_buffer_manager.html#aa9f557181f36180a9f9f71a50f141f2b", null ],
    [ "reset", "classe_h_s_m_1_1_buffer_manager.html#ad855f564a62bfb23125935a3c7d40d8b", null ],
    [ "write", "classe_h_s_m_1_1_buffer_manager.html#a452ba9bb34081afa3844d3632e1eea07", null ],
    [ "write", "classe_h_s_m_1_1_buffer_manager.html#a2e36ae0c33594200dc10044e9b7d0851", null ]
];