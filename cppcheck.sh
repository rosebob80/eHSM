#!/bin/bash
xml_report_name=cppcheck_report.xml
report_path=reports/cppcheck
# [[ -d ./build ]] && rm -rf ./build
mkdir -p build
cd build
cmake .. -G Ninja -DCPPCHECK_XML_OUTPUT=$xml_report_name
ninja cppcheck-analysis