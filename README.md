# eHSM Library

eHSM is an acronym for **e**mbedded **H**ierarchical **S**tate **M**achine. This library provides a class that implements the State Pattern, and a collection of utility classes.

Provides:

- Template Based Ring Buffer, Array and DMA Buffer Containers, with static memory allocation using [this pattern](https://youtu.be/TYqbgvHfxjM?t=1999)
- Delegate class and Qt-ish Signal class, sort of [this](http://doc.qt.io/archives/qt-4.8/signalsandslots.html), but way more simplified.
- [Hierarchical State Machine](https://barrgroup.com/Embedded-Systems/How-To/Introduction-Hierarchical-State-Machines)

## Supported Compilers

Tested with gcc for x84 and ARM architectures, but should work in any architecture with a decent C++03 support.

## Building

eHSM is a header-only library. Consequently, no build process is required for the library itself.

However, eHSM includes CMake scripts for supporting the following actions:

- Building the unit tests.
- Building the sample programs.

## Unit Testing

eHSM requires Google Test for unit testing.

## Examples

See the Examples folder.

## Documentation

See https://ecruzolivera.gitlab.io/eHSM/
