#include <stdio.h>

#include <cstdint>

#define MAX_NEST_DEPTH 5
#include "HierarchicalStateMachine.hpp"

const int STATE_MAX_EVENTS = 2;
using namespace std;
using namespace eHSM;

void UnlockingNotification(int event)
{
  printf("UnlockingNotification event %i\r\n", event);
}

void LockingNotification(int event)
{
  printf("UnlockingNotification event %i\r\n", event);
}

void OnEnteringLocked()
{
  printf("OnEnteringLocked\r\n");
}

void OnExitingLocked()
{
  printf("OnExitingLocked\r\n");
}

void OnEnteringUnLocked()
{
  printf("OnEnteringUnLocked\r\n");
}

void OnExitingUnLocked()
{
  printf("OnExitingUnLocked\r\n");
}

int main()
{
  enum Events
  {
    CoinInserted,
    BarPushed
  };
  Declare::State<STATE_MAX_EVENTS> locked;
  Declare::State<STATE_MAX_EVENTS> unlocked;
  HierarchicalStateMachine         hsm;
  // Binding actions to events
  State::Action_t actionTemp;
  actionTemp.bind<UnlockingNotification>();
  locked.addEvent(CoinInserted, unlocked, actionTemp);
  actionTemp.bind<LockingNotification>();
  unlocked.addEvent(BarPushed, locked, actionTemp);
  // Connecting Entering and Exiting Signals
  locked.signalEntered.connect<OnEnteringLocked>();
  locked.signalExited.connect<OnExitingLocked>();
  unlocked.signalEntered.connect<OnEnteringUnLocked>();
  unlocked.signalExited.connect<OnExitingUnLocked>();

  hsm.setInitialState(locked);
  hsm.start();
  hsm.dispatch(BarPushed);
  hsm.dispatch(CoinInserted);
  hsm.dispatch(CoinInserted);
  hsm.dispatch(BarPushed);
}
