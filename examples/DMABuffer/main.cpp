#include <cstdint>
#include <cstdio>

#include "DmaBuffer.hpp"
using namespace std;

void FillDmaBuffer(eHSM::DmaBuffer<uint8_t> *localDmaBufferPtr, uint8_t value)
{
  while(!localDmaBufferPtr->isFull())
  {
    localDmaBufferPtr->write(value);
  }
}

bool CheckDmaBuffer(eHSM::DmaBuffer<uint8_t> *localDmaBufferPtr, uint8_t value)
{
  for(uint32_t index = 0; index < localDmaBufferPtr->size(); index++)
  {
    if(localDmaBufferPtr->at(index) != value)
    {
      return false;
    }
  }
  return true;
}

int main()
{
  const uint8_t VALUE_TO_FILL1 = 55;
  const uint8_t VALUE_TO_FILL2 = 99;
  // are actually different type of classes, DmaBuffer<uint8_t, 16> and
  // DmaBuffer<uint8_t, 32>
  eHSM::Declare::DmaBuffer<uint8_t, 16> uint8x16DmaBuffer;
  eHSM::Declare::DmaBuffer<uint8_t, 32> uint8x32DmaBuffer;
  // Nevertheless You can use the same pointer (DmaBuffer<uint8_t> *) to both
  // classes
  FillDmaBuffer(&uint8x16DmaBuffer, VALUE_TO_FILL1);
  FillDmaBuffer(&uint8x32DmaBuffer, VALUE_TO_FILL2);

  if(CheckDmaBuffer(&uint8x16DmaBuffer, VALUE_TO_FILL1))
  {
    printf("Ok\r\n");
  }
  else
  {
    printf("FAIL\r\n");
  }

  if(CheckDmaBuffer(&uint8x32DmaBuffer, VALUE_TO_FILL2))
  {
    printf("Ok\r\n");
  }
  else
  {
    printf("FAIL\r\n");
  }
}
