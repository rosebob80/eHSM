/*******************************************************************************
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Ernesto Cruz Olivera (ecruzolivera@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 *all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

/**
 ******************************************************************************
 * @file    State.h
 * @author  Ing. Manuel Alejandro Linares Paez
 * @version 1.0
 * @date    Apr 12, 2016
 * @brief   Base class for every state in the hierarchical state machine
 * @bug     No known bugs.
 ******************************************************************************
 */

#ifndef STATE_HPP
#define STATE_HPP

#include <cstdint>

#include "Signal.hpp"
#include "Utils.hpp"

namespace eHSM
{
/**
 * @brief The State class is the base class for all the states in the HSM.
 * This class can not be instantiated directly, it can only be used to declare
 * pointers.
 * To create instances use eHSM::Declare::State<>
 */
class State
{
  friend class HierarchicalStateMachine;

 public:
  virtual ~State() {}

  /**
   * @brief EventId_t
   * The events are just plain intereger which size is architecture dependant.
   */
  typedef int EventId_t;

  /**
   * @brief Action_t
   * The action to an event handling is a delegate to a void (int) function.
   */
  typedef Delegate<int> Action_t;

  /**
   * @brief StateSignal_t
   * The signals emmited from the state can be connected to a void (void)
   * function.
   */
  typedef Signal<void, 1> StateSignal_t;

  /**
   * @brief Return type for the handler function of the state
   */
  enum EventHandlerType
  {
    EventIgnored,               // The event does nothing
    TriggerAction,              // The event triggered an action
    TriggerTransition,          // The event triggered a transition
    TriggerActionAndTransition  // The event triggered a action and a transition
  };

  /**
   * @brief superstate
   * @return A pointer to the parent state
   */
  State *superstate() const
  {
    return superstatePtr_;
  }

  /**
   * @brief Sets the parent state of this state
   * @param superstate Parent state
   */
  void setSuperstate(State &superstate)
  {
    superstatePtr_ = &superstate;
  }

  /**
   * @brief Adds a transition and an action to handle one event.
   * It does nothing if the event is allready handled
   * @param event Event for which there is a transition and action
   * @param transition State target of the transition
   * @param action Action to execute. This must be a global or static function
   * with
   * no return value and no parameters
   * @return True if the event could be added
   * False if the event could not be added
   */
  bool addEvent(EventId_t event, State &nextState, Action_t &action)
  {
    if((action.isUnbinded()) || eventListPtr_->isFull() || isEventHandled(event))
    {
      return false;
    }
    Event temp = {event, &nextState, action};
    eventListPtr_->append(temp);
    return true;
  }

  /**
   * @brief Adds a transition to handle one event.
   * It does nothing if the event is allready handled
   * @param event Event for which there is a transition
   * @param transition State target of the transition
   * @return True if the event could be added
   * False if the event could not be added
   */
  bool addEvent(EventId_t event, State &nextState)
  {
    if(eventListPtr_->isFull() || isEventHandled(event) || &nextState == this)
    {
      return false;
    }
    Action_t emptyDelegate;
    Event    temp = {event, &nextState, emptyDelegate};
    eventListPtr_->append(temp);
    return true;
  }

  /**
   * @brief Adds an action to handle one event.
   * It does nothing if the event is allready handled
   * @param event Event for which there is an action
   * @param action Action to execute. This must be a global or static function
   * with
   * no return value and no parameters
   * @return True if the event could be added
   * False if the event could not be added
   */
  bool addEvent(EventId_t event, Action_t &action)
  {
    if((action.isUnbinded()) || eventListPtr_->isFull() || isEventHandled(event))
    {
      return false;
    }

    Event temp = {event, E_NULLPTR, action};
    eventListPtr_->append(temp);
    return true;
  }

  /**
   * @brief Deletes an event from the handled events list
   * @param event Event to delete
   * @return True if the event could be deleted
   * False if the event could not be deleted
   */
  bool removeEvent(EventId_t event)
  {
    int index = findEvent(event);
    if(index == -1)
    {
      return false;
    }
    eventListPtr_->remove(index);
    return true;
  }

  /**
   * @brief Checks if an event is allready handled by the state
   * @param event Event
   * @return True if the event is handled
   * False if the event is not handled
   */
  bool isEventHandled(EventId_t eventId)
  {
    int index = findEvent(eventId);
    return index != -1;
  }

  /**
   * @brief Deletes all events and its transitions and actions handled by the
   * state
   */
  void removeAllEvents()
  {
    eventListPtr_->clear();
  }

  /**
   * @brief Signal triggered when the state is exited.
   * Only one slot can be connected and must have no return value and no
   * parameters.
   * To connect a slot to this signal proceed as shown:
   *
   * @code
   * eHSM::Declare::State<5> state;
   * state.signalExited.connect<&function>();
   * @endcode
   */
  StateSignal_t signalExited;

  /**
   * @brief Signal triggered when the state is entered.
   * Only one slot can be connected and must have no return value and no
   * parameters.
   * To connect a slot to this signal proceed as shown:
   *
   * @code
   * eHSM::Declare::State<5> state;
   * state.signalEntered.connect<&function>();
   * @endcode
   */
  StateSignal_t signalEntered;

  /**
   * @brief initialStatePtr
   * @return Pointer to the initial substate of this state
   */
  eHSM::State *initialState() const
  {
    return initialStateSubStatePtr_;
  }

  /**
   * @brief Sets the initial state of this state. The superstate of initial
   * state must be this state
   * @param Reference to the initial substate
   */
  void setInitialSubState(eHSM::State &initialState)
  {
    assert(this == initialState.superstate());
    initialStateSubStatePtr_ = &initialState;
  }

  /**
   * @brief hasInitialSubState
   * @return True if this state has an initial state
   */
  bool hasInitialSubState() const
  {
    return initialStateSubStatePtr_ != E_NULLPTR;
  }

  /**
   * @brief hasDefaultEvent
   * @return True if this state has a default event configured. False otherwise
   */
  bool hasDefaultEvent() const
  {
    return defaultEvent_.eventId != 0;
  }

  /**
   * @brief Adds a transition and an action to handle the default event
   * @param nextState State target of the transition
   * @param action Action to execute
   */
  void setDefaultEvent(State &nextState, Action_t &action)
  {
    defaultEvent_.eventId       = 1;
    defaultEvent_.action        = action;
    defaultEvent_.nextStatePtr_ = &nextState;
  }

  /**
   * @brief Adds a transition to handle the default event
   * @param nextState State target of the transition
   */
  void setDefaultEvent(State &nextState)
  {
    defaultEvent_.eventId = 1;
    Action_t emptyDelegate;
    defaultEvent_.action        = emptyDelegate;
    defaultEvent_.nextStatePtr_ = &nextState;
  }

  /**
   * @brief Adds an action to handle the default event
   * @param action Action to execute
   */
  void setDefaultEvent(Action_t &action)
  {
    defaultEvent_.eventId       = 1;
    defaultEvent_.action        = action;
    defaultEvent_.nextStatePtr_ = E_NULLPTR;
  }

  /**
   * @brief Removes the default event action and/or transition
   */
  void removeDefaultEvent()
  {
    defaultEvent_.eventId = 0;
    Action_t emptyDelegate;
    defaultEvent_.action        = emptyDelegate;
    defaultEvent_.nextStatePtr_ = E_NULLPTR;
  }

 protected:
  /**
   * @brief Struct to save the event and the transition/action that triggers
   */
  struct Event
  {
    EventId_t eventId;
    State *   nextStatePtr_;
    Action_t  action;
  };

  /**
   * @brief Search for an event in the handled events list
   * @param event Event
   * @return The index of the event in the list
   * -1 if the event is not in the list
   */
  int findEvent(EventId_t event)
  {
    for(std::uint32_t i = 0; i < eventListPtr_->itemsAviable(); i++)
    {
      if(eventListPtr_->at(i).eventId == event)
      {
        return i;
      }
    }
    return -1;
  }

  /**
   * @brief Handles an incoming event
   * @param event Event
   * @return Unhandled if the state doesn't carry any action or transition
   * Handled if the state carry an action
   * Transition if the state must carry transition to another state
   */
  EventHandlerType handleEvent(EventId_t eventId)
  {
    if(!isEventHandled(eventId))
    {
      return EventIgnored;
    }
    EventHandlerType retval = EventIgnored;
    Event            event  = eventListPtr_->at(findEvent(eventId));

    if(event.nextStatePtr_ != E_NULLPTR && !event.action.isUnbinded())
    {
      event.action(eventId);
      nextStatePtr_ = event.nextStatePtr_;
      retval        = TriggerActionAndTransition;
    }
    else if(event.nextStatePtr_ != E_NULLPTR)
    {
      nextStatePtr_ = event.nextStatePtr_;
      retval        = TriggerTransition;
    }
    else if(!event.action.isUnbinded())
    {
      event.action(eventId);
      retval = TriggerAction;
    }
    return retval;
  }

  /**
   * @brief Handles the default event
   * @return Unhandled if the state don't carry any action or transition
   * Handled if the state carry an action
   * Transition if the state must carry transition to another state
   */
  EventHandlerType handleDefaultEvent(EventId_t eventId)
  {
    if(!hasDefaultEvent())
    {
      return EventIgnored;
    }
    EventHandlerType retval = EventIgnored;

    if(defaultEvent_.nextStatePtr_ != E_NULLPTR && !defaultEvent_.action.isUnbinded())
    {
      defaultEvent_.action(eventId);
      nextStatePtr_ = defaultEvent_.nextStatePtr_;
      retval        = TriggerActionAndTransition;
    }
    else if(defaultEvent_.nextStatePtr_ != E_NULLPTR)
    {
      nextStatePtr_ = defaultEvent_.nextStatePtr_;
      retval        = TriggerTransition;
    }
    else if(!defaultEvent_.action.isUnbinded())
    {
      defaultEvent_.action(eventId);
      retval = TriggerAction;
    }
    return retval;
  }

  /**
   * @brief This empty function is called every time the state is entered.
   * The user can reimplement to do entry actions
   */
  virtual void onEntry() {}

  /**
   * @brief This empty function is called every time the state is exited.
   * The user can reimplement to do exit actions
   */
  virtual void onExit() {}
  /**
   * @brief Protected contructor that can only be used by derived classes
   * @param ptr Pointer to the derived class internal array
   */
  explicit State(Array<Event> *ptr)
      : superstatePtr_(E_NULLPTR), eventListPtr_(ptr), nextStatePtr_(E_NULLPTR), initialStateSubStatePtr_(E_NULLPTR)
  {
    defaultEvent_.eventId       = 0;
    defaultEvent_.nextStatePtr_ = E_NULLPTR;
    Action_t emptyDelegate;
    defaultEvent_.action = emptyDelegate;
  }

  /**
   * @brief Pointer to the superstate
   */
  eHSM::State *superstatePtr_;

  /**
   * @brief Pointer to the list of handled events
   */
  Array<Event> *eventListPtr_;

  /**
   * @brief Pointer to the next state
   */
  eHSM::State *nextStatePtr_;

  /**
   * @brief Pointer to the initial substate if any
   */
  eHSM::State *initialStateSubStatePtr_;

  /**
   * @brief Default event information
   */
  Event defaultEvent_;

  E_DISABLE_COPY(State);

};  // State

namespace Declare
{
/**
 * @brief Class used to instantiate the objects
 * @tparam MAX_EVENTS_HANDLED Maximum number of events that the state can handle
 */
template <std::uint32_t MAX_EVENTS_HANDLED>
class State : public ::eHSM::State
{
 public:
  /**
   * @brief Public constructor
   */
  State() : eHSM::State(&eventList_) {}

 private:
  Declare::Array<Event, MAX_EVENTS_HANDLED> eventList_;
  E_DISABLE_COPY(State);

};  // State

}  // namespace Declare

}  // namespace eHSM

#endif  // STATE_HPP
